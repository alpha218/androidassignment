package com.example.zolomovie.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.example.zolomovie.R

class MovieProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_profile)

        val name = intent.getStringExtra("name")
        val tv = findViewById<TextView>(R.id.textView2)
        tv.text = name
        val imageId = intent.getIntExtra("imageId",0)
        val iv = findViewById<ImageView>(R.id.imageView)
        iv.setImageResource(imageId)

        val about = intent.getStringExtra("about")
        val aboutV = findViewById<TextView>(R.id.textView3)
        aboutV.text = about

        val backBtn = findViewById<Button>(R.id.button)
        backBtn.setOnClickListener(){
            val navigate = Intent(this, MainActivity:: class.java)
            startActivity(navigate)
        }

        val ratingValue = findViewById<RatingBar>(R.id.ratingBar2)
        val rating = intent.getFloatExtra("rating",0.0f)
        ratingValue.rating = rating






    }
}