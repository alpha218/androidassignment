package com.example.zolomovie.Views

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zolomovie.Adapters.Adapter
import com.example.zolomovie.Adapters.MainAdapter
import com.example.zolomovie.Model.DataModel
import com.example.zolomovie.Model.MainModel
import com.example.zolomovie.OnMovieClickListener
import com.example.zolomovie.R

class MainActivity : AppCompatActivity(), OnMovieClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val rvMain = findViewById<RecyclerView>(R.id.rvMain)
//        val rvchild = findViewById<RecyclerView>(R.id.rvMovieChild)
//        rvchild.adapter = Adapter(getData(),this)
//        rvchild.layoutManager = LinearLayoutManager(this,RecyclerView.HORIZONTAL,false)
        rvMain.adapter = MainAdapter(getAllData())
        rvMain.layoutManager = LinearLayoutManager(this,RecyclerView.VERTICAL,false)




        val rv = findViewById<RecyclerView>(R.id.recyclerView)
        rv.layoutManager = LinearLayoutManager(this,RecyclerView.HORIZONTAL,false)


        var adapter = Adapter(getData(),this)
        rv.adapter = adapter
        adapter.notifyDataSetChanged()




    }

    private fun getAllData() : ArrayList<MainModel>{
        var list  = ArrayList<MainModel>()
        var movieList = ArrayList<DataModel>()
        var movieList2 = ArrayList<DataModel>()

        var movieModel = DataModel("Dark Knight", R.drawable.movie1,"jkwenfwk",2.1f)
        var movieModel2 = DataModel("Dark Knight", R.drawable.movie2,"jkwenfwk",2.1f)
        var movieModel3 = DataModel("Dark Knight", R.drawable.movie2,"jkwenfwk",2.1f)
        var movieModel4 = DataModel("Dark Knight", R.drawable.movie3,"jkwenfwk",2.1f)
        var movieModel5 = DataModel("No Strings Attached", R.drawable.movie4,"jkwenfwk",2.1f)
        movieList.add(movieModel)
        movieList.add(movieModel2)
        movieList.add(movieModel3)
        movieList.add(movieModel4)
        movieList2.add(movieModel)
        movieList2.add(movieModel2)
        movieList2.add(movieModel3)
        movieList2.add(movieModel4)
        movieList2.add(movieModel5)

        var model = MainModel("Action",movieList)
        var model2 = MainModel("RomCom", movieList2)
        list.add(model2)
        list.add(model)
        return list
    }



    private  fun getData() : ArrayList<DataModel>{
        var list = ArrayList<DataModel>()

        var model = DataModel("Dark Knight", R.drawable.movie1,"jkwenfwk",2.1f)
        list.add(DataModel("Superman", R.drawable.movie2,"fkwejn",2.5f))
        list.add(DataModel("Harry Potter", R.drawable.movie3,"mkwfejnf",2.6f))
        list.add(DataModel("Harry Potter", R.drawable.movie3,"fnwkejnf",4.1f))
        list.add(model)

        return list
    }

    override fun onMovieItemClicked(position: Int) {

        var moviesList = getData()

        val intent = Intent(this, MovieProfileActivity::class.java)
        intent.putExtra("name", moviesList[position].text)
        intent.putExtra("imageId",moviesList[position].imageId)
        intent.putExtra("about",moviesList[position].about)
        intent.putExtra("rating",moviesList[position].rating)


        startActivity(intent)





    }


}