package com.example.zolomovie.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.zolomovie.Model.DataModel
import com.example.zolomovie.R

class MovieAdapter (var list :ArrayList<DataModel>) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    class MovieViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        val movieImage : ImageView = itemView.findViewById(R.id.movieImage)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_item,parent,false)
        return MovieViewHolder(view)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.movieImage.setImageResource(list[position].imageId)
    }

}