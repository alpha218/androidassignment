package com.example.zolomovie.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.zolomovie.Model.DataModel
import com.example.zolomovie.OnMovieClickListener
import com.example.zolomovie.R

class Adapter (var list :ArrayList<DataModel>, private val onMovieClickListener: OnMovieClickListener) : RecyclerView.Adapter<Adapter.ViewHolder>() {

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        var itemName : TextView = itemView.findViewById(R.id.itemName)
        var itemImage: ImageView = itemView.findViewById(R.id.gridImage)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.grid_item,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemName.text  = list[position].text
        holder.itemImage.setImageResource(list[position].imageId)



        holder.itemView.setOnClickListener {
            onMovieClickListener.onMovieItemClicked(position)

        }


    }

    override fun getItemCount(): Int {
        return list.size
    }




}
