package com.example.zolomovie.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zolomovie.Model.MainModel
import com.example.zolomovie.OnMovieClickListener
import com.example.zolomovie.R

class MainAdapter (val collection : ArrayList<MainModel>) : RecyclerView.Adapter<MainAdapter.CollectionViewHolder>(),
    OnMovieClickListener {

    class CollectionViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        var genreTitle : TextView = itemView.findViewById(R.id.genreView)
        var rvMovieChild : RecyclerView = itemView.findViewById(R.id.rvMovieChild)





    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollectionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.parent_item,parent,false)
        return CollectionViewHolder(view)
    }

    override fun getItemCount(): Int {
        return collection.size
    }

    override fun onBindViewHolder(holder: CollectionViewHolder, position: Int) {
        val collection = collection[position]
        holder.genreTitle.text = collection.title


        val movieAdapter = MovieAdapter(collection.movieModels)
        holder.rvMovieChild.adapter = movieAdapter
        holder.rvMovieChild.setHasFixedSize(true)
        holder.rvMovieChild.layoutManager = LinearLayoutManager(holder.itemView.context,RecyclerView.HORIZONTAL,false)

    }

    override fun onMovieItemClicked(position: Int) {

    }


}